## Table Of Contents

<!-- vim-markdown-toc GFM -->

* [Climate](#climate)
* [Cycling](#cycling)
* [Ethical, Privacy & Security](#ethical-privacy--security)
* [Gaming](#gaming)
* [Hardware](#hardware)
* [Health & Productivity](#health--productivity)
* [Information & Resources](#information--resources)
* [Computers & Technology](#computers--technology)
* [Computer Keyboards](#computer-keyboards)
* [The Web & the Internet](#the-web--the-internet)
* [Software](#software)
    * [Git](#git)
    * [Linux, BSD & Open Source Software](#linux-bsd--open-source-software)
    * [XMPP](#xmpp)
    * [Vi and derivatives](#vi-and-derivatives)

<!-- vim-markdown-toc -->

## Climate

1. [SV] [Klädernas miljö- och klimatpåverkan](https://www.naturskyddsforeningen.se/skola/energifallet/faktablad-vara-klader) (2019)
2. [The carbon cost of digital tech](https://foundry4.com/the-carbon-cost-of-digital-tech) (2019)
3. [Energy Conservation with Open Source Ad Blockers](https://www.mdpi.com/2227-7080/8/2/18) (2020)
4. [Which Programming Languages Use the Least Electricity?](https://thenewstack.io/which-programming-languages-use-the-least-electricity/) (2018)
5. [CO2 emissions on the web](https://dannyvankooten.com/website-carbon-emissions/) (2020)
6. [Bitcoin consumes more energy than Switzerland, according to new estimate](https://www.theverge.com/2019/7/4/20682109/bitcoin-energy-consumption-annual-calculation-cambridge-index-cbeci-country-comparison) (2019)
7. [Bitcoin’s energy consumption is underestimated: A market dynamics approach](https://www.sciencedirect.com/science/article/abs/pii/S2214629620302966?via%3Dihub) (2020)
8. [Another thing you may not know about Bitcoin: it's killing the planet](https://www.theguardian.com/commentisfree/2019/jan/17/bitcoin-big-oil-environment-energy) (2019)
9. [Can the planet really afford the exorbitant power demands of machine learning?](https://www.theguardian.com/commentisfree/2019/nov/16/can-planet-afford-exorbitant-power-demands-of-machine-learning) (2019)
10. [YouTube could help the planet by throwing out its digital waste](https://www.theverge.com/2019/5/7/18531107/youtube-waste-energy-power-video-streaming-google) (2019)
11. [Environmental aspects of the electric car](https://en.m.wikipedia.org/wiki/Environmental_aspects_of_the_electric_car)
12. [The only two satellites dedicated to observing the north and south poles are almost certain to die before replacements are flown](https://www.bbc.co.uk/news/science-environment-55109092) (2020)
13. [Climate Change: Bitcoin Is an Incredibly Dirty Business](https://www.bloomberg.com/opinion/articles/2021-01-26/is-bitcoin-mining-worth-the-environmental-cost) (2021)
14. [Bitcoin emissions alone could push global warming above 2°C](https://www.nature.com/articles/s41558-018-0321-8) (2018)
15. [Bitcoin may soon consume more power than Australia — almost 10 times more than Google, Microsoft and Facebook combined](https://www.abc.net.au/news/2021-03-18/bitcoin-has-a-climate-problem/13210376) (2021)
16. [Report: Bitcoin Mining May Require More Electricity Than Three Quarters of the World's Nations](https://www.livebitcoinnews.com/report-bitcoin-mining-may-require-more-energy-than-180-countries/) (2021)

## Cycling

1. [Cycle commuters live longer says 15 year study of people's mobility habits](https://road.cc/content/news/270743-cycle-commuters-live-longer-says-15-year-study-peoples-mobility-habits) -Road Bike Rider Cycling
2. [Don’t Straighten Your Knees While Running or Cycling](https://www.roadbikerider.com/dont-straighten-knees-running-cycling-2/) - Road Bike Rider Cycling
3. [Ride of the year? Cyclist completes Transcontinental on Brompton folding bike](https://road.cc/content/news/cyclist-completes-transcontinental-brompton-295395) - Bike.cc
4. [Fred Schmid, 89, is bound to finish Unbound Gravel](https://www.velonews.com/culture/fred-schmid-89-is-bound-to-finish-unbound-gravel/) - VeloNews.com
5. [What is the carbon footprint of your bike?](https://road.cc/content/feature/carbon-footprint-bike-294925) - Road.c
6. [E-bikes can do wonders for the environment, as shown in new Cycle Republic infographics](https://ebiketips.road.cc/content/news/e-bikes-can-do-wonders-for-the-environment-as-shown-in-new-cycle-republic-infographics) - eBikeTips
7. [Why You Should Take It Easy & Enjoy Cycling Slowly!](https://youtu.be/RUXUcL3vf8w) - YouTube

9. [Apple’s Self-Repair Program Manages to Make MacBooks Seem Less Repairable](https://www.ifixit.com/News/64072/apples-self-repair-program-manages-to-make-macbooks-seem-less-repairable) - iFixit
10. [You won't be able to run Windows 11 without a webcam [in 2023]](https://windowsreport.com/windows-11-webcam-mandatory/) - Windows Report
 
## Ethical, Privacy & Security

1. [Can We Build Trustable Hardware?](https://www.bunniestudios.com/blog/?p=5706) (2019)
2. [Three Reasons Why the "Nothing to Hide" Argument is Flawed](https://spreadprivacy.com/three-reasons-why-the-nothing-to-hide-argument-is-flawed/) (2018)
3. [A Future Without Privacy](https://danielmiessler.com/blog/future-without-privacy/) (2019)
4. [RMS: "Saying No to unjust computing even once is helpful"](https://www.fsf.org/blogs/community/rms-article-201csaying-no-to-unjust-computing-even-once-is-helpful201d) (2020)
5. [SV] [Det här är integritet – och därför behöver du bry dig](https://internetkunskap.se/integritet/det-har-ar-integritet-och-darfor-behover-du-bry-dig/) 
6. [All the Ways Facebook Tracks You](https://www.wired.com/story/ways-facebook-tracks-you-limit-it/) (2020)
7. [How Facebook Tracks You, Even When Not on Facebook](https://www.consumerreports.org/privacy/how-facebook-tracks-you-even-when-youre-not-on-facebook/) (2018)
8. [So what is the encryption in 3G/4G networks?](https://medium.com/asecuritysite-when-bob-met-alice/so-what-is-the-encryption-in-3g-4g-networks-139b8c0da3eb) (2019)
9. [Privacy matters even if "you have nothing to hide" ](https://write.privacytools.io/freddy/why-privacy-matters-even-if-you-have-nothing-to-hide) (2019)
10. [Wiping harddisks in 2019](https://daniel-lange.com/archives/157-Wiping-harddisks-in-2019.html) (2019)
11. [Disqus, the dark commenting system](https://supunkavinda.blog/disqus) (2021)
12. [I don't trust Signal](https://drewdevault.com/2018/08/08/Signal.html) (2018)
13. [Over 25% Of Tor Exit Relays Spied On Users' Dark Web Activities](https://thehackernews.com/2021/05/over-25-of-tor-exit-relays-are-spying.html) (2021)
14. [Cross-browser tracking vulnerablity in Tor, Safari, Chrome and Firefox](https://fingerprintjs.com/blog/external-protocol-flooding/) (2021)
15. [Whatsapp? No thanks, I prefer to have control over my data](https://www.nicfab.it/whatsapp-no-thanks-i-prefer-to-have-control-over-my-data/) (2021)
16. [Why you shouldn't trust Discord](https://cadence.moe/blog/2020-06-06-fuckdiscord) (2020)
 
## Gaming

1. [Seed for "most iconic image in Minecraft history" found after eight month search](https://www.eurogamer.net/articles/2020-09-05-seed-for-most-iconic-image-in-minecraft-history-found-after-eight-month-search) (2020)

## Hardware

1. [Introducing Precursor - A mobile, open source electronics platform](https://www.bunniestudios.com/blog/?p=5921) (2020)
2. [When coffee makers are demanding a ransom, you know IoT is screwed](https://arstechnica.com/information-technology/2020/09/how-a-hacker-turned-a-250-coffee-maker-into-ransom-machine/) (2020)
3. [Five-bay, open source NAS features RK3399, UPS, and 2.5GbE](http://linuxgizmos.com/five-bay-open-source-nas-features-rk3399-ups-and-2-5gbe/) (2020)
4. [Best SBCs for NAS Use](https://www.electromaker.io/blog/article/best-sbcs-for-nas-use) (2020
5. [ThinkPad on a Budget: Core i-series ThinkPads](https://youtu.be/EisG4sOSNKw) (2018)
6. [The Definitive T430 Modding Guide](https://medium.com/@n4ru/the-definitive-t430-modding-guide-3dff3f6a8e2e) (2018)

## Health & Productivity

1. [A New, More Rigorous Study Confirms: The More You Use Facebook, the Worse You Feel](https://hbr.org/2017/04/a-new-more-rigorous-study-confirms-the-more-you-use-facebook-the-worse-you-feel) (2017)
2. [Is Facebook Luring You Into Being Depressed?](http://nautil.us/issue/31/stress/is-facebook-luring-you-into-being-depressed) (2015)
3. [Is Dark Mode Actually Better For Your Eyes?](https://www.youtube.com/watch?v=bCaFRN3aaP8)
4. [Think You're Multitasking? Think Again](https://www.npr.org/templates/story/story.php?storyId=95256794?storyId=95256794&t=1610868195303) (2008)
5. [Multiple-Monitor Productivity: Fact or Fiction?](https://dubroy.com/blog/multiple-monitor-productivity-fact-or-fiction/) (2018)
6. [Why I Stopped Using Multiple Monitors](https://hackernoon.com/why-i-stopped-using-multiple-monitors-bfd87efa2e5b) (2017)
7. [News is the Last Thing We Need Right Now](https://www.raptitude.com/2020/12/news-is-the-last-thing-we-need-right-now/) (2020)

## Information & Resources

1. [How to choose a green web host](https://www.wholegraindigital.com/blog/choose-a-green-web-host/) (2020)
2. [Hundred Rabbits — off the grid](https://100r.co/site/off_the_grid.html) - Thoughts and resources about living off the grid.
3. [The Hitchhiker’s Guide to Online Anonymity](https://anonymousplanet.github.io/thgtoa/guide.html)
4. [Ten Quick Tips for Staying Safe Online](https://github.com/gvwilson/10-safety/)
 
## Computers & Technology

1. [Why email is the best discussion platform](https://www.paritybit.ca/blog/why-email-is-the-best-discussion-platform) (2020)
2. [Why IRC is Still Good in $CURRENT_YEAR](https://www.paritybit.ca/blog/why-irc-is-still-good) (2020)
3. [Discovering Two Screens Aren’t Better Than One](https://www.nytimes.com/2014/03/20/technology/personaltech/surviving-and-thriving-in-a-one-monitor-world.html) (2014)
4. [Please use text/plain for email](https://drewdevault.com/2016/04/11/Please-use-text-plain-for-emails.html) (2016)
5. [A Week with an AlphaStation: The Old Computer Challenge](https://kernelpanic.life/misc/week-with-an-alphastation.html)

## Computer Keyboards

1. [A Beginner’s Guide to Switches](https://www.theremingoat.com/blog/beginners-guide) (2020)
2. [A Beginner’s Guide to Force Curves](https://www.theremingoat.com/blog/a-beginners-guide-to-force-curves) (2020)
3. [The Problem with Mechanical Switch Reviews](https://deskthority.net/viewtopic.php?f=62&start=&t=15133) (2016)
4. [How to Read a Force Curve](https://www.keebtalk.com/t/how-to-read-a-force-curve/65) (2018)
5. [Installing a RGB mechanical keypad on my microwave](https://github.com/dekuNukem/pimp_my_microwave/) (2020)
6. [The lies you've been told about the origin of the QWERTY keyboard](https://www.theatlantic.com/technology/archive/2013/05/the-lies-youve-been-told-about-the-origin-of-the-qwerty-keyboard/275537/) (2013)
7. [DataHand Keyboard](https://octopup.org/computer/datahand) (2012)

## The Web & the Internet

1. [Obese websites and planet-sized metronomes](https://kevingal.com/blog/metronome.html) (2020)
2. [We rendered a million web pages to find out what makes the web slow](https://catchjs.com/Blog/PerformanceInTheWild) (2020)
3. [The unreasonable effectiveness of simple HTML](https://shkspr.mobi/blog/2021/01/the-unreasonable-effectiveness-of-simple-html/)
4. [Principles for Making Websites](https://letra.studio/principles/)

## Software

1. [Building a new Win 3.1 app in 2019 Part 1: Slack client](https://yeokhengmeng.com/2019/12/building-a-new-win-3-1-app-in-2019-part-1-slack-client/) (2019)
2. [Scientists rename human genes to stop Microsoft Excel from misreading them as dates](https://www.theverge.com/2020/8/6/21355674/human-genes-rename-microsoft-excel-misreading-dates) (2020)
 
### Git

1. [Learn to use email with git](https://git-send-email.io/)
2. [Linus Torvalds don't do GitHub pull requests](https://github.com/torvalds/linux/pull/17#issuecomment-5654674) (2012)
3. [Git email flow vs Github flow](https://blog.brixit.nl/git-email-flow-versus-github-flow/) (2020)

### Linux, BSD & Open Source Software

1. [The open source movement runs on the heroic efforts of not enough people doing too much work. They need help.](https://www.wired.com/story/open-source-coders-few-tired/) (2020)
2. [Endangered Firefox: The state of Mozilla](https://www.zdnet.com/article/endangered-firefox-the-state-of-mozilla/) (2020)
3. [Linux Hardening Guide](https://madaidans-insecurities.github.io/guides/linux-hardening.html) (2020)
4. [Become shell literate](https://drewdevault.com/2020/12/12/Shell-literacy.html) (2020)
5. [pfSense vs OPNsense](https://teklager.se/en/pfsense-vs-opnsense/)
6. [Ledger CLI cheatsheet](https://devhints.io/ledger)
7. [Problems with Systemd and Why I like BSD Init](https://textplain.net/blog/2015/problems-with-systemd-and-why-i-like-bsd-init/) (2015)
8. [The real motivation behind systemd](https://unixsheikh.com/articles/the-real-motivation-behind-systemd.html) (2018)
9. [Why you should migrate everything from Linux to BSD](https://www.unixsheikh.com/articles/why-you-should-migrate-everything-from-linux-to-bsd.html) (2020)
 
### XMPP

1. [XMPP Explained - Extensible Messaging & Presence Protocol](https://getstream.io/blog/xmpp-extensible-messaging-presence-protocol/)
2. [How I Bridged iMessage and XMPP](https://chrisbeckstrom.com/posts/How-I-Bridged-iMessage-and-XMPP/) (2020)
3. [Instant Messaging: It's not about the app](https://xmpp.org/2021/01/instant-messaging-its-not-about-the-app/) (2021)
4. [It’s all about choices and control](https://xmpp.org/2015/01/its-all-about-choices-and-control/) (2015)

### Vi and derivatives

1. [Popular Vim Commands - Comprehensive Vim Cheat Sheet](https://www.keycdn.com/blog/vim-commands) (2020)
2. [Vim-like Layer for Xorg and Wayland](https://cedaei.com/posts/vim-like-layer-for-xorg-wayland/) (2020)
3. [Learn vim For the Last Time: A Tutorial and Primer](https://danielmiessler.com/study/vim/) (2020)
4. [Coming home to vim](https://stevelosh.com/blog/2010/09/coming-home-to-vim/) (2010)
5. [Vim quick reference card](http://tnerual.eriogerg.free.fr/vimqrc.html)
6. [Your problem with Vim is that you don't grok vi](https://stackoverflow.com/questions/1218390/what-is-your-most-productive-shortcut-with-vim/1220118#1220118)
7. [Everyone Who Tried to Convince Me to use Vim was Wrong](https://yehudakatz.com/2010/07/29/everyone-who-tried-to-convince-me-to-use-vim-was-wrong/) (2010)
8. [Why, oh WHY, do those #?@! nutheads use vi?](http://www.viemu.com/a-why-vi-vim.html) (2007)
9. [How I boosted my Vim](https://nvie.com/posts/how-i-boosted-my-vim/) (2010)
10. [Graphical vi-vim Cheat Sheet and Tutorial](http://www.viemu.com/a_vi_vim_graphical_cheat_sheet_tutorial.html)
11. [An Extremely Quick and Simple Introduction to the Vi Text Editor](http://heather.cs.ucdavis.edu/~matloff/UnixAndC/Editors/ViIntro.html) (2006)
12. [VIM Quick reference card](http://tnerual.eriogerg.free.fr/vim.html)
13. [A Vim Guide for Advanced Users](https://thevaluable.dev/vim-advanced/) (2021)
14. [History and effective use of Vim](https://begriffs.com/posts/2019-07-19-history-use-vim.html) (2019)
